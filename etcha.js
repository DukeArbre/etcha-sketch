var size;
var pad_size= 600;
$(document).ready(function() {
	newSize();
});

function reset() {
	$(".square").css("background-color", "black");
}

function newSize() {
	$("#container").remove();
	$("button").remove();

	//Change the grid size
	size = prompt("What size size should the grid be? (1-30)");
	if (size > 30) {
		alert("Max size is 30.");
		size = 30;
	} else if (size < 1) {
		alert("Minimum size is 1.");
		size = 1;
	} else if (size == 7){
		size = 8;
		//7 can't create a nice looking grid.
	}

	//Creates a grid with the specified size
	$('body').append('<div id="container"></div>');
	$("#container").css({
		width: pad_size,
		height: pad_size,
		margin: '20px auto',
		'text-align': 'center'
		});

	for(var i = 0; i < size * size; i++) {
		$('#container').append('<div class="square"></div>');
	}	
	$(".square").css({
		float: 'left',
		width: pad_size/size,
		height: pad_size/size
	});

	$("#container").append("<button onclick='reset(); return false'>Reset</button>");
	$("#container").append("<button onclick='newSize(); return false'>New Size</button>");

	reset();
	$(".square").hover(function() {
			$(this).css("background-color", "#00ff00");
		})
}